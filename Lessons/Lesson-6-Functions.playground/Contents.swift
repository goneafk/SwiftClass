// Lesson 6 - Controls and Errors
// How to control flow and then to report errors

import Cocoa

// In a playground you have to define all functions above the cdoe that will use them

// function to write a file to /tmp

func printSerial() {
    
    let text = "Swift is by far the easiest language to learn"
    
    do {
    try text.write(to: URL.init(string: "/tmp/test.txt")!, atomically: true, encoding: String.Encoding.utf8)
    } catch {
        print("ruh roh")
        return
    }
    
    print("Done!")
}

// function to take a string and make an alert

func makeAlert(message: String ) {
    
    let notification = NSUserNotification()
    
    notification.title = "Setup notification"
    notification.informativeText = message
    NSUserNotificationCenter.default.deliver(notification)
}

// func to take an upper bound and then generate a random number and return it

func getRandom(upper: Int) -> Int {
    
    return Int(arc4random_uniform(UInt32(upper)))
}

// Now to call the functions

// No arguments, no return

printSerial()

// An argument, no return

print("Background the playground... quick!")
sleep(5)

makeAlert(message: "This is cool!")

// Arguments and return

print(getRandom(upper: 10))

// Exercises

// 1. Make a function that takes two strings as input and returns them combined
// 2. Make a function that takes a string and returns the first and last word as a tuple
// 3. Make a function that takes a dictionary and a search term and then returns an optional



























/*

// Answers

func stringCombiner( first: String, second: String ) -> String {
    return (first + " " + second)
}

func firstAndLast( text: String ) -> ( String, String ) {
    let array = text.components(separatedBy: " ")
    return (array.first!, array.last!)
}

func checkDictionary( dict: [ String : String], term: String) -> String? {
    
    return dict[term]
}

// now to call the functions

let answerOne = stringCombiner(first: "Swift", second: "rocks")
print(answerOne)

let (first, last) = firstAndLast(text: "This has been a great class")
print(first)
print(last)

let cities = [
    "Vancouver" : "YYZ",
    "San Franciso" : "SFO",
    "Austin" : "AUS",
    "Peoria" : "PIA",
    "San Jose" : "SJC",
    "Orlando" : "MCO",
    "Chicago" : "ORD"
]

print(checkDictionary(dict: cities, term: "Ottowa") as Any)
 */
